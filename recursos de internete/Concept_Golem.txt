Recurso sacado de: https://www.artstation.com/artwork/3oyarY

Se ha realizado una búsqueda a través de internet y, el arte presentado para el golem en esta ilustración casa con el segundo de la fila de abajo. Será el diseño seleccionado, aunque se realizarán cambios para ajustarlo del todo a tamaño y estilo gráfico, además de su desarrollo en sprites.



Los sprites de losgolems han sido sacados de la pagina "The Spriters Resource" tras una búsqueda en diversas páginas. No han sido cogidos como tal, si no que lo que se ha hecho ha sido tomarlos a modo de referencia.






matthieu-michon-golem1